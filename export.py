import MySQLdb
import os
import csv

host = os.environ['mysql_host']
user = os.environ['mysql_user']
passwd = os.environ['mysql_pass']

db = MySQLdb.connect(host,user,passwd,"licitacoes")

cursor = db.cursor()
cursor.execute("SELECT cidade, url, novo FROM licitacoes INNER JOIN cidades ON licitacoes.cidade_id = cidades.id ")
cursor.fetchall()

header = [["cidade", "url", "novo"]]

myFile = open('licitacoes.csv', 'w')
with myFile:
    writer = csv.writer(myFile)
    writer.writerows(header)
    writer.writerows(cursor)

cursor.execute("update licitacoes set novo = 0")
db.commit()